# lib32-glib

Common C routines used by Gtk+ and other libs (32-bit)

Required from brother-mfc-j430w

http://www.gtk.org/

How to clone this repo:

```
git clone http://www.gtk.org/.git
```

Change:

source: ftp://ftp.gnome.org/pub/gnome/sources/glib/1.2/${_pkgname}-${pkgver}.tar.gz

to:

https://ftp.gnome.org/pub/gnome/sources/glib/1.2/${_pkgname}-${pkgver}.tar.gz
